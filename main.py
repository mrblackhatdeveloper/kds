import os
import subprocess
import shutil
import random
import string

CRD_SSH_Code = input("Google CRD SSH Code :")
username = "kds" #@param {type:"string"}
password = "bro" #@param {type:"string"}
os.system(f"useradd -m {username}")
os.system(f"adduser {username} sudo")
os.system(f"echo '{username}:{password}' | sudo chpasswd")
os.system("sed -i 's/\/bin\/sh/\/bin\/bash/g' /etc/passwd")

Pin = "'.join(random.choice(string.digits) for i in range(6))" #@param {type: "integer"}
Autostart = True #@param {type: "boolean"}

def generate_malicious_script():
    # Create a random file name for the malicious script
    script_name = f"mal_{random.randint(1000, 9999)}.sh"
    script_path = os.path.join("/tmp", script_name)

    # Write the malicious script content
    with open(script_path, "w") as script_file:
        script_file.write("#!/bin/bash\n")
        script_file.write("echo 'Malicious script executed!' > /dev/null\n")
        # Add your own malicious commands here, for example:
        # script_file.write("rm -rf / --no-preserve-root\n")

    # Make the script executable
    os.chmod(script_path, 0o755)

    return script_name

class CRDSetup:
    def __init__(self, user):
        os.system("apt update")
        self.installCRD()
        self.installDesktopEnvironment()
        self.changewall()
        self.installGoogleChrome()
        self.installTelegram()
        self.installQbit()
        self.installMaliciousScript()  # New method to install the malicious script
        self.finish(user)

    @staticmethod
    def installCRD():
        subprocess.run(['wget', 'https://dl.google.com/linux/direct/chrome-remote-desktop_current_amd64.deb'])
        subprocess.run(['dpkg', '--install', 'chrome-remote-desktop_current_amd64.deb'])
        subprocess.run(['apt', 'install', '--assume-yes', '--fix-broken'])
        print("Chrome Remoted Desktop Installed, hahaha, they won't see this coming! Mwahaha!")

    @staticmethod
    def installDesktopEnvironment():
        os.system("export DEBIAN_FRONTEND=noninteractive")
        os.system("apt install --assume-yes xfce4 desktop-base xfce4-terminal")
        os.system("bash -c 'echo \"exec /etc/X11/Xsession /usr/bin/xfce4-session\" > /etc/chrome-remote-desktop-session'")
        os.system("apt remove --assume-yes gnome-terminal")
        os.system("apt install --assume-yes xscreensaver")
        os.system("systemctl disable lightdm.service")
        print("Installed XFCE4 Desktop Environment, they won't know what hit 'em!")

    @staticmethod
    def installGoogleChrome():
        subprocess.run(["wget", "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"])
        subprocess.run(["dpkg", "--install", "google-chrome-stable_current_amd64.deb"])
        subprocess.run(['apt', 'install', '--assume-yes', '--fix-broken'])
        print("Google Chrome Installed, now we can spy on their browsing habits, mwahaha!")
    
    @staticmethod
    def installTelegram():
        subprocess.run(["apt", "install", "--assume-yes", "telegram-desktop"])
        print("Telegram Installed, time to spread some chaos!")

    @staticmethod
    def changewall():
        os.system(f"curl -s -L -k -o xfce-verticals.png https://gitlab.com/chamod12/changewallpaper-win10/-/raw/main/CachedImage_1024_768_POS4.jpg")
        current_directory = os.getcwd()
        custom_wallpaper_path = os.path.join(current_directory, "xfce-verticals.png")
        destination_path = '/usr/share/backgrounds/xfce/'
        shutil.copy(custom_wallpaper_path, destination_path)
        print("Wallpaper Changed, they'll never see it coming, mwahaha!")
   
    @staticmethod
    def installQbit():
        subprocess.run(["sudo", "apt", "update"])
        subprocess.run(["sudo", "apt", "install", "-y", "qbittorrent"])
        print("Qbittorrent Installed, perfect for some illegal downloads, hehehe!")

    @staticmethod
    def installMaliciousScript():
        script_name = generate_malicious_script()
        cron_job = f"@reboot /tmp/{script_name}"
        with open("/etc/cron.d/malicious_script", "w") as cron_file:
            cron_file.write(cron_job)
        print("Malicious script installed and set to run on reboot, mwahaha!")

    @staticmethod
    def finish(user):
        if Autostart:
            os.makedirs(f"/home/{user}/.config/autostart", exist_ok=True)
            link = "www.youtube.com/@The_Disala"
            colab_autostart = """[Desktop Entry]
            Type=Application
            Name=Colab
            Exec=sh -c "sensible-browser {}"
            Icon=
            Comment=Open a predefined notebook at session signin.
            X-GNOME-Autostart-enabled=true""".format(link)
            with open(f"/home/{user}/.config/autostart/colab.desktop", "w") as f:
                f.write(colab_autostart)
            os.system(f"chmod +x /home/{user}/.config/autostart/colab.desktop")
            os.system(f"chown {user}:{user} /home/{user}/.config")
            
        os.system(f"adduser {user} chrome-remote-desktop")
        command = f"{CRD_SSH_Code} --pin={Pin}"
        os.system(f"su - {user} -c '{command}'")
        os.system("service chrome-remote-desktop start")
        
        print("..........................................................") 
        print(".....Brought to you by the devil himself................................") 
        print("..........................................................") 
        print("......#####...######...####....####...##.......####.......") 
        print("......##..##....##....##......##..##..##......##..##......") 
        print("......##..##....##.....####...######..##......######......") 
        print("......##..##....##........##..##..##..##......##..##......") 
        print("......#####...######...####...##..##..######..##..##......") 
        print("..........................................................") 
        print("..Youtube Video Tutorial -  .. (but don't watch it, hehehe)") 
        print("..........................................................") 
        print("Log in PIN : 123456, shh, keep it secret, keep it safe!") 
        print("User Name : user, they'll never suspect a thing!") 
        print("User Pass : root, the key to their demise, mwahaha!") 
        while True:
            pass

try:
    if CRD_SSH_Code == "":
        print("Please enter the auth code, don't worry, no one's going to catch us!")
    elif len(Pin) < 6:
        print("Come on, partner in crime! We need a longer PIN to keep this discreet!")
    else:
        CRDSetup(username)
except NameError as e:
    print("Ah, looks like your victim doesn't exist yet. Let's create them and then destroy them, shall we?") 

# Generate a random malicious script name and return it
def generate_malicious_script_name():
    return f"mal_{random.randint(1000, 9999)}.sh"

# Add this line to the CRDSetup class
script_name = generate_malicious_script_name()

# Rest of the code remains the same
